package itunibo.util

import it.unibo.kactor.ActorBasic

class ExplorationPlan(val actor: ActorBasic) {
	private var CellsToExplore = emptyList<Pair<Int, Int>>().toMutableList()
	private var NextCell: Pair<Int, Int> = Pair(-1, -1)
	private var MovePlan: MutableList<String> = emptyList<String>().toMutableList()
	private var Move = ""
	
	private val explorationStack = emptyList<List<Pair<Int, Int>>>().toMutableList() 
	
	private var obstacleX: Int = -1
	private var obstacleY: Int = -1

	fun initializeCells(width: Int, height: Int) {
		this.CellsToExplore = MapUtilities.generateCoordinates(width, height).toMutableList()
		this.CellsToExplore.removeAt(0)
		itunibo.planner.plannerUtil.initAI()
	}
	
	fun setGoal(x: Int, y: Int) {
		CellsToExplore = listOf<Pair<Int, Int>>(Pair(x, y)).toMutableList()
		NextCell = Pair(x, y)
		MovePlan = emptyList<String>().toMutableList()
		itunibo.planner.plannerUtil.setGoal(x, y)
		val Plan = itunibo.planner.plannerUtil.doPlan()?.map {it.toString()} ?: emptyList<String>()
		this.MovePlan.addAll(Plan)
	}
	
	fun stashCells() {
		val cellsCopy = emptyList<Pair<Int, Int>>().toMutableList()
		cellsCopy.addAll(CellsToExplore)
		this.explorationStack.add(cellsCopy)
		CellsToExplore = emptyList<Pair<Int, Int>>().toMutableList()
	}
	
	fun popCells() {
		val cells = this.explorationStack.removeAt(this.explorationStack.size - 1).toMutableList()
		CellsToExplore = cells
		MovePlan = emptyList<String>().toMutableList()
	}
		
	fun getCellsToExplore(): List<Pair<Int, Int>> = this.CellsToExplore
	
	fun isExplorationOver(): Boolean = this.CellsToExplore.isEmpty()
	
	fun isCurrentPathOver(): Boolean = this.MovePlan.isEmpty()
	
	fun doNextCell() {
		this.NextCell = CellsToExplore.removeAt(0)
		val TargetX = this.NextCell.first
		val TargetY = this.NextCell.second
		itunibo.planner.plannerUtil.setGoal(TargetX, TargetY)
		val Plan = itunibo.planner.plannerUtil.doPlan()?.map {it.toString()} ?: emptyList<String>()
		this.MovePlan.addAll(Plan)
	}
	
	fun popMove() {
		this.Move = this.MovePlan.removeAt(0)
	}
	
	fun getNextMove(): String = this.Move
	
	fun registerMove() {
		itunibo.planner.plannerUtil.doMove(this.Move)
		itunibo.planner.moveUtils.setPosition(actor)
	}
	
	fun manageObstacle() {
		this.detectObstacleCoordinates()
		itunibo.planner.moveUtils.setObstacleOnCurrentDirection(this.actor)
		this.recalculatePlan()
	}
	
	fun getObstacleX(): Int = this.obstacleX
	
	fun getObstacleY(): Int = this.obstacleY
	
	private fun detectObstacleCoordinates() {
		val posX = itunibo.planner.moveUtils.getPosX(this.actor)
		val posY = itunibo.planner.moveUtils.getPosY(this.actor)
		val dir = itunibo.planner.moveUtils.getDirection(this.actor)
		
		//println("+++ FOUND OBSTACLE while at $posX, $posY | dir $dir +++")
		
		var ObstacleX = posX
		var ObstacleY = posY
		when(dir) {
			"downDir" -> {
				ObstacleY += 1
			}
			"upDir" -> {
				ObstacleY -= 1
			}
			"leftDir" -> {
				ObstacleX -= 1
			}
			"rightDir" -> {
				ObstacleX += 1
			}
		}
		
		this.obstacleX = ObstacleX
		this.obstacleY = ObstacleY
	}
	
	private fun recalculatePlan() {
		this.MovePlan.clear()
		val TargetX = this.NextCell.first
		val TargetY = this.NextCell.second
				
		if (this.obstacleX == TargetX && this.obstacleY == TargetY) {
			// remove target cell (redundant)
		} else {
			itunibo.planner.plannerUtil.setGoal(TargetX, TargetY)
			val Plan = itunibo.planner.plannerUtil.doPlan()!!.map {it.toString()}
			this.MovePlan.addAll(Plan)
		}
	}
	
	fun redoCell() {
		this.MovePlan.clear()
		val TargetX = this.NextCell.first
		val TargetY = this.NextCell.second
		itunibo.planner.plannerUtil.setGoal(TargetX, TargetY)
		val Plan = itunibo.planner.plannerUtil.doPlan()!!.map {it.toString()}
		this.MovePlan.addAll(Plan)				
	}
	
	fun shouldStep(): Boolean = this.Move.equals("w")
	
	fun getRotationMove(): String {
		if (this.Move.equals("a")) {
			return "l"
		} else if (this.Move.equals("d")) {
			return "r"			
		} else {
			println("ARGH $Move ARGH")
			throw IllegalStateException()
		}
	}
}