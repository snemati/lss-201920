import it.unibo.kactor.ActorBasic
import it.unibo.kactor.sysUtil
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.Assert.assertEquals

class TestExploreIsSystematic {
	
	companion object {
		val exploreTimeSeconds = 20
	}
	
	var mapactor: ActorBasic? =  null
	
	@Before
	@Throws(Exception::class)
	fun systemSetUp() {
		GlobalScope.launch {
			log("Starting explore")
			it.unibo.ctxDetector.main()
		}
		
		Thread.sleep(5000)
		
		mapactor = sysUtil.getActor("mapresource")
		
	}
	
	@Test
	fun exploreTest() {
		Thread.sleep(exploreTimeSeconds * 1000L)
		ensureRoomIsFullyExplored()
	}
	
	fun ensureRoomIsFullyExplored() {
		
		assertEquals("'1 1 1 r @1 1 1 1 @1 1 1 1 @1 1 1 1 @X 0 X @'",
			 itunibo.planner.plannerUtil.getMapOneLine())
	}
	
	@After
	fun sysTerminate() {
		log("Terminating explore")
	}
	
	fun log(msg: String) = println("*** $msg ***")
}