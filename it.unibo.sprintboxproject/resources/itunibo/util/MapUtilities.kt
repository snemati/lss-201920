object MapUtilities {
	fun generateCoordinates(width: Int, height: Int): List<Pair<Int, Int>> {
		val result = emptyList<Pair<Int, Int>>().toMutableList()
		for (x in 0..width-1) {
			for (y in 0..height-1) {
				result.add(Pair(x, y))
			}
		}
		return result
	}
}