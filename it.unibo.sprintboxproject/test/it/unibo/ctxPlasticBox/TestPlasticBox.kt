package it.unibo.ctxPlasticBox

import org.junit.Before
import org.junit.Test
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import it.unibo.kactor.ActorBasic
import it.unibo.kactor.sysUtil
import org.junit.Assert.assertEquals

class TestPlasticBox {
	companion object {
		val TEST_PERIOD_SECONDS = 10L
		val TEST_PERIOD_MS = TEST_PERIOD_SECONDS * 1000L
		
		val INITIAL_BOX_CONTENT = "0"
		val FINAL_BOX_CONTENT = "10"
		
		val INITIAL_SUPERVISOR_SIGNAL = "false"
		val FINAL_SUPERVISOR_SIGNAL = "true"
	}
	
	var plasticBox: ActorBasic? = null
	var supervisor: ActorBasic? = null
	
	@Before
	@Throws(Exception::class)
	fun setUp() {
		GlobalScope.launch {
			it.unibo.ctxPlasticBox.main()
		}
		
		Thread.sleep(2000)
		
		plasticBox = sysUtil.getActor("plasticbox")
		supervisor = sysUtil.getActor("supervisormock")
	}
	
	@Test
	fun testPlasticBox() {
		ensureBoxIsEmpty()
		ensureSupervisorIsNotAlerted()
		Thread.sleep(TEST_PERIOD_MS)
		ensureBoxIsFull()
		ensureSupervisorIsAlerted()
	}
	
	fun ensureBoxIsEmpty() {
		with(plasticBox!!) {
			solve("content(N)", "N")
			if (solveOk()) {
				assertEquals(INITIAL_BOX_CONTENT.toString(), resVar)
			}
		}
	}
	
	fun ensureBoxIsFull() {
		with(plasticBox!!) {
			solve("content(N)", "N")
			if (solveOk()) {
				assertEquals(FINAL_BOX_CONTENT.toString(), resVar)
			}
		}
	}
	
	fun ensureSupervisorIsNotAlerted() {
		with(supervisor!!) {
			solve("signal(S)", "S")
			if (solveOk()) {
				assertEquals(INITIAL_SUPERVISOR_SIGNAL, resVar)
			}
		}
	}
	
	fun ensureSupervisorIsAlerted() {
		with(supervisor!!) {
			solve("signal(S)", "S")
			if (solveOk()) {
				assertEquals(FINAL_SUPERVISOR_SIGNAL, resVar)
			}
		}
	}
	
}