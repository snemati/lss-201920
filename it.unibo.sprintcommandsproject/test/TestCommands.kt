package it.unibo.ctxDetector

import it.unibo.kactor.ActorBasic
import it.unibo.kactor.sysUtil
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.junit.Before
import org.junit.Test
import org.junit.Assert.*

class TestCommands {
	companion object {
		val TEST_TIME_SECONDS = 20L
		val TEST_TIME_MS = TEST_TIME_SECONDS * 1000L
	}
	
	var detector: ActorBasic? = null
	var supervisor: ActorBasic? = null
	
	@Before
	@Throws(Exception::class)
	fun setUp() {
		GlobalScope.launch {
			it.unibo.ctxDetector.main()
		}
		
		Thread.sleep(5000)
		
		detector = sysUtil.getActor("detector")
		supervisor = sysUtil.getActor("supervisor")
	}
	
	@Test
	fun testCommands() {
		Thread.sleep(TEST_TIME_MS)
		ensureExploreIsCorrect()
		ensureSuspendIsCorrect()
		ensureTerminateIsCorrect()
	}
	
	fun ensureExploreIsCorrect() {
		with(detector!!) {
			testKB("state_log(explore, final_position(X, _))", "X", "0")
			testKB("state_log(explore, final_position(_, Y))", "Y", "0")			
			testKB("state_log(explore, detector_box(N))", "N", "0")
			testKB("state_log(explore, plastic_box(N))", "N", "5")
			testKB("state_log(explore, wroom_plastic(N))", "N", "0")
		}
	}
	
	fun ensureSuspendIsCorrect() {
		with(detector!!) {
			testKB("state_log(goHome, final_position(X, _))", "X", "0")
			testKB("state_log(goHome, final_position(_, Y))", "Y", "0")
		}
	}
	
	fun ensureTerminateIsCorrect() {
		with(detector!!) {
			testKB("state_log(terminateWork, final_position(X, _))", "X", "0")
			testKB("state_log(terminateWork, final_position(_, Y))", "Y", "0")			
			testKB("state_log(terminateWork, detector_box(N))", "N", "0")
			testKB("state_log(terminateWork, plastic_box(N))", "N", "3")
			testKB("state_log(terminateWork, wroom_plastic(N))", "N", "2")
		}
	}
	
	fun ActorBasic.testKB(query: String, variable: String, expected: String) {
		solve(query, variable)
		assertTrue(solveOk())
		assertEquals(expected, resVar)
	}
}