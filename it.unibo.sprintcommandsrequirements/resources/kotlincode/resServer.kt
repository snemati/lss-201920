package kotlincode

import org.eclipse.californium.core.CoapServer
import it.unibo.kactor.ActorBasic

object resServer{
		fun init(owner: ActorBasic, port: Int){
			val server = CoapServer(port);
			server.add( 
				 resPlasticBox(owner,  "plasticBox").add(	//robot
					 resPlasticBoxItems(owner, "items"))  //robot/pos, robot/sonar
			)
			server.start();			
		}
}
