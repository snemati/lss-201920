const express     	= require('express');
const path         	= require('path');
const logger       	= require('morgan');	//see 10.1 of nodeExpressWeb.pdf;
const bodyParser   	= require('body-parser');
const fs           	= require('fs');
const index         = require('./appServer/routes/index');				 

const mqttUtils     = require('./uniboSupports/mqttUtils');  
const qakevh       = require('./uniboSupports/qakeventHandler');
const coap          = require('./uniboSupports/coapClientToSupervisor');  

let app              = express();
var connectionSocket;

app.set('views', path.join(__dirname, 'appServer', 'views'));	 
app.set('view engine', 'ejs');
var accessLogStream = fs.createWriteStream(path.join(__dirname, 'morganLog.log'), {flags: 'a'})
app.use(logger("short", {stream: accessLogStream}));
app.use(logger('dev'));				//shows commands, e.g. GET /pi 304 23.123 ms - -;
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'jsCode')));


app.setIoSocket = function(iosock){
	connectionSocket = iosock;
	qakevh.setIoSocket(iosock);
	console.log("THE SETIOSOCKET FUNCTION HAS BEEN INVOKED");
}

//ROUTES
app.get('/info', function (req, res) {
	res.send('applCode | This is the frontend-Unibo!')
  });
  
app.get('/', function(req, res) {
	res.render("index");
});	

//FromQAK post
app.post('/map', function(req, res) {
	let content = req.body.map
	connectionSocket.emit('map', {map: content});
	res.sendStatus(200)
});

app.post('/detector', function(req, res) {
	let content = req.body.detector
	connectionSocket.emit('detectorbox', {spaceLeft: content})
	res.sendStatus(200)
});

app.post('/testbug', function(req, res) {
	let content = req.body.test
	connectionSocket.emit('test', {test: content})
	res.sendStatus(200)
});

app.post('/stuck', function(req, res) {
	let content = req.body.detector
	connectionSocket.emit('stuck', {stuck: content})
	res.sendStatus(200)
});

app.post('/isPlastic', function(req, res) {
	let content = req.body.plastic
	connectionSocket.emit('isPlastic', {plastic: content})
	res.sendStatus(200)
});

app.post('/ready', function(req, res) {
	var coapAddr = "coap://localhost:5683"
    coap.setcoapAddr( coapAddr )
	res.sendStatus(200)
});

//From Frontend Post
app.post("/explore", function(req, res) {
	parameterlessDispatch("explore", "SupervisorJs", "worldobserver")
	res.sendStatus(204)
});

app.post("/suspend", function(req, res) {
	parameterlessDispatch("suspend", "SupervisorJs", "worldobserver")
	res.sendStatus(204)
});

app.post("/terminate", function(req, res) { 
	parameterlessDispatch("terminate", "SupervisorJs", "worldobserver")
	res.sendStatus(204)
});

app.post("/tvochigh", function(req, res) { 
	stateSensorEvent("sensor", "SupervisorJs", "TVOC", "high")
	res.sendStatus(204)
});

app.post("/tvoclow", function(req, res, next) { 
	stateSensorEvent("sensor", "SupervisorJs", "TVOC", "low")
	res.sendStatus(204)
});

app.post("/roomclosed", function(req, res, next) { 
	stateSensorEvent("sensor", "SupervisorJs", "Room", "closed")
	res.sendStatus(204)
});

app.post("/roomopen", function(req, res, next) { 
	stateSensorEvent("sensor", "SupervisorJs", "Room", "open")
	res.sendStatus(204)
});

app.post("/yesplastic", function(req, res) { 
	parameterlessReply("plastic", "SupervisorJs", "detector", "plastic")
	res.sendStatus(204)
});

app.post("/noplastic", function(req, res) { 
	parameterlessReply("notPlastic", "SupervisorJs", "detector", "notPlastic")
	res.sendStatus(204)
});

function parameterlessDispatch(messageName, from, to) {
	let communication = "msg(" + messageName + ", dispatch," + from + ", " + to + ", " + messageName + "(x), 1)";
	console.log("Dispatching " + communication + " to QAK");
	mqttUtils.publish(communication, "unibo/qak/" + to);
}

function stateSensorEvent(eventName, from, sensorName, sensorState) {
	let communication = "msg(" + eventName + ", event," + from + ", none, " + eventName + "(" + sensorName + ", " + sensorState + "),1)"; 
	console.log("Emitting " + communication + " to QAK");
	mqttUtils.publish(communication, "unibo/qak/events");
}

function parameterlessReply(replyName, from, to, reply) {
	let communication = "msg(" + replyName + ", reply," + from + "," + to + ", " + reply + "(x),1)"; 
	console.log("Replying with " + communication + " to QAK");
	mqttUtils.publish(communication, "unibo/qak/" + to);
}

app.use(function(req, res){
	try{
		// if (req.accepts('json')) {
		// 	return res.send(result);		
		// } else {
		// 	return res.render('index' );
		// }

		//NO: we lose the message sent via socket.io
		//return res.render('index' );  
	}catch(e){
		console.log("exception " + e );
	}
});

app.use(function(req, res, next) {
	console.log("APPLCODE 404")
	var err = new Error('Not Found');
	err.status = 404;
	next(err);
});

app.use(function(err, req, res, next) {
	console.log("APPLCODE 500")

	res.locals.message = err.message;
	res.locals.error = req.app.get('env') === 'development' ? err : {};
	res.status(err.status || 500);
	res.render('error');
});

module.exports = app;