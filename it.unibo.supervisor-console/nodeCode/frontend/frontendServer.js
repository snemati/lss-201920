const coap 		= require('./uniboSupports/coapClientToSupervisor');  
var appl  	= require('./applCode');
var http   	= require('http');

let io; 	
let port = 8080;

let createServer = function(port) {
	console.log("CREATESERVER HAS BEEN INVOKED")

	let server = http.createServer(appl);   
	io = require('socket.io').listen(server);

	//server.on('listening', onListening);
	//server.on('error', onError);
	server.listen(port);

	// io.sockets.on('connection', function(socket) {
	// 	socket.on('room', function(room) {
	// 		socket.join(room);
	// 	});
	// });

	//var io = require('socket.io').listen(9030);
	io.sockets.on('connection', function(socket) {
		let socketToFrontendCLient = socket
		appl.setIoSocket(socketToFrontendCLient);
	});
};

main();

function main() {     
    createServer(port);     
}

// function onListening() {
// 	var addr = server.address();
// 	var bind = (typeof addr === 'string' ? ('pipe ' + addr) : ('port ' + addr.port));
// 	console.log("ONLISTENING")
// }

// function onError(error) {
// if (error.syscall !== 'listen') {
// throw error;
// }
// var bind = typeof port === 'string'
// ? 'Pipe ' + port
// : 'Port ' + port;
// // handle specific listen errors with friendly messages;
// switch (error.code) {
// case 'EACCES':
// console.error(bind + ' requires elevated privileges');
// process.exit(1);
// break;
// case 'EADDRINUSE':
// console.error(bind + ' is already in use');
// process.exit(1);
// break;
// default:
// throw error;
// }
// }