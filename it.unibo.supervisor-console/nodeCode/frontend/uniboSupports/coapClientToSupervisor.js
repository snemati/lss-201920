const handle           = require('./qakeventHandler');  
const coap             = require("node-coap-client").CoapClient; 
//var coapAddr           = "coap://192.168.1.8:5683"	//RESOURCE ON RASPBERRY PI
var coapAddr             = "coap://localhost:5683"



function createCoapClient( resourceAddr ){
    console.log("		coapClientToResourceModel | createCoapClient "  );
    coap
        .observe(
            resourceAddr /* string */,
            "get" /* "get" | "post" | "put" | "delete" */,
            handle.handeData //handeData /* function */
            //[payload /* Buffer */,]
            //[options /* RequestOptions */]
        )
        .then(() => { console.log("		coapClientToResourceModel | observe setup on" + resourceAddr ); /* observing was successfully set up */})
        .catch(err => { console.log("		coapClientToResourceModel | observe error " + err )  /* handle error */ })
        ;
    }

     
exports.setcoapAddr = function ( addr ){
	//coapAddr = "coap://"+ addr + ":5683";
	//coapResourceAddr   = coapAddr + "/robot/pos" // coap://localhost:5683/robot/pos
	
	plasticBoxAddr = addr + "/plasticBox/items"
	console.log("coap | coapResourceAddr=" + plasticBoxAddr);
	createCoapClient( plasticBoxAddr   );
}

exports.coapGet = function (  ){
	coap
	    .request(
	         coapResourceAddr,
	        "get" /* "get" | "post" | "put" | "delete" */
 	        //[payload /* Buffer */,
	        //[options /* RequestOptions */]]
	    )
		.then(response => { 			/* handle response */
			handle.handeData
	    	console.log("coap get done> " + response.payload );}
	     )
	    .catch(err => { /* handle error */ 
	    	console.log("coap get error> " + err );}
	    )
	    ;
	    
}//coapPut

exports.coapPut = function (  cmd ){ 
console.log("PUT " + coapResourceAddr);
	coap
	    .request(
	        coapResourceAddr,     
	        "put" ,			                          // "get" | "post" | "put" | "delete"   
	        new Buffer(cmd )                          // payload Buffer 
 	        //[options]]							//  RequestOptions 
	    )
	    .then(response => { 			// handle response  
	    	console.log("coap put done> " + cmd + " " + response);} 
	     )
	    .catch(err => { // handle error  
	    	console.log("coap | put error> " + err + " for cmd=" + cmd);}
	    )
	    ;
	    
}//coapPut

// const myself   = require('./coapClientToSupervisor');

// function test(){
//  	//console.log("GET");
//   	myself.coapGet();
//  	//console.log("PUT");
//  	myself.coapPut("r")
//  	myself.coapGet();
// }

// //test()
 

/*
 * ========= EXPORTS =======
 */

//module.exports = coap;
