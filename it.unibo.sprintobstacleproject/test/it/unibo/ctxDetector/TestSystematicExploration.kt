package it.unibo.ctxDetector

import org.junit.Before
import it.unibo.kactor.ActorBasic
import it.unibo.kactor.sysUtil
import org.junit.Test
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.junit.Assert.assertEquals

class TestSystematicExploration {
		
	companion object {
		val EXPLORATION_WAIT_MS = 45 * 1000L
	}
	
	var detector: ActorBasic? = null
	
	var roomHeight: Int? = null
	var roomWidth: Int? = null
	
	@Before
	@Throws(Exception::class)
	fun setUp() {
		GlobalScope.launch {
			it.unibo.ctxDetector.main()
		}
		
		Thread.sleep(2000)
		
		detector = sysUtil.getActor("detector")
	}
	
	@Test
	fun testSystematicExploration() {
		readRoomDimensions()
		ensureCellsAreNotVisited()
		Thread.sleep(EXPLORATION_WAIT_MS)
		ensureCellsAreVisited()
		ensureObstaclesAreDetected()
	}
	
	fun readRoomDimensions() {
		with(detector!!) {
			solve("roomHeight(H)", "H")
			if (solveOk()) {
				roomHeight = resVar.toInt()
			}
			solve("roomWidth(W)", "W")
			if (solveOk()) {
				roomWidth = resVar.toInt()
			}
		}
	}
	
	fun ensureCellsAreNotVisited() {
		for (y in 0..roomHeight!!-1) {
			for (x in 0..roomWidth!!-1) {
				with(detector!!) {
					solve("cell($y, $x, S)", "S")
					if (solveOk()) {
						assertEquals("0", resVar)
					}
				}
			}
		}
	}
	
	fun ensureCellsAreVisited() {
		for (y in 0..roomHeight!!-1) {
			for (x in 0..roomWidth!!-1) {
				with(detector!!) {
					if (!isObstacle(x, y)) {
						solve("cell($y, $x, S)", "S")
						if (solveOk()) {
							assertEquals("1", resVar)
						}
					}
				}
			}
		}
	}
		
	fun ensureObstaclesAreDetected() {
		for (y in 0..roomHeight!!-1) {
			for (x in 0..roomWidth!!-1) {
				with(detector!!) {
					if (isObstacle(x, y)) {
						solve("cell($y, $x, S)", "S")
						if (solveOk()) {
							assertEquals("2", resVar)
							println("" + y + x + resVar)
						}
					}
				}
			}
		}
	}
		
	fun isObstacle(x: Int, y: Int): Boolean =
		(y == 0 && x == 1) ||
		(y == 0 && x == 2) ||
		(y == 0 && x == 4) ||
		(y == 4 && x == 4)
				
}