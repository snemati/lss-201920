package it.unibo.ctxDetector

import it.unibo.kactor.ActorBasic
import it.unibo.kactor.sysUtil
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.junit.Before
import org.junit.Test
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue

class TestEmptyDetectorBox {
	companion object {
		val EXPLORATION_TIME_SEC = 60L
		val EXPLORATION_TIME_MS = EXPLORATION_TIME_SEC * 1000L
		
		val FINAL_PLASTIC_BOX_BOTTLES = 3
	}
	
	var detector: ActorBasic? = null
	var plasticBox: ActorBasic? = null
	
	var roomHeight: Int? = null
	var roomWidth: Int? = null
	
	@Before
	@Throws(Exception::class)
	fun setUp() {
		GlobalScope.launch {
			it.unibo.ctxDetector.main()
		}
		
		Thread.sleep(5000)
		
		detector = sysUtil.getActor("detector")
		plasticBox = sysUtil.getActor("plasticbox")
	}
	
	@Test
	fun testEmptyDetectorBox() {
		readRoomDimensions()
		
		Thread.sleep(EXPLORATION_TIME_MS)
		
		ensureRoomIsExplored()
		ensureDetectorIsAtHome()
		ensureDetectorBoxIsEmpty()
		ensurePlasticBoxContainsObjects()
	}
		
	fun readRoomDimensions() {
		with(detector!!) {
			solve("roomHeight(H)", "H")
			if (solveOk()) {
				roomHeight = resVar.toInt()
			}
			solve("roomWidth(W)", "W")
			if (solveOk()) {
				roomWidth = resVar.toInt()
			}
		}
	}
	
	fun ensureRoomIsExplored() {
		for (y in 0..roomHeight!!-1) {
			for (x in 0..roomWidth!!-1) {
				with(detector!!) {
					solve("cell($y, $x, S)", "S")
					if (solveOk()) {
						assertEquals("1", resVar)
					}
				}
			}
		}
	}
	
	fun ensureDetectorIsAtHome() {
		with(detector!!) {
			solve("curPos(X, _)", "X")
			assertTrue(solveOk())
			assertEquals("0", resVar)
			solve("curPos(_, Y)", "Y")
			assertTrue(solveOk())
			assertEquals("0", resVar)
		}
	}
	
	fun ensureDetectorBoxIsEmpty() {
		with(detector!!) {
			solve("detector_box(N)", "N")
			assertTrue(solveOk())
			assertEquals("0", resVar)
		}
	}
	
	fun ensurePlasticBoxContainsObjects() {
		with(plasticBox!!) {
			solve("content(N)", "N")
			assertTrue(solveOk())
			assertEquals(FINAL_PLASTIC_BOX_BOTTLES.toString(), resVar)
		}
	}
}