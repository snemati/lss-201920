package it.unibo.ctxDetector

import it.unibo.kactor.ActorBasic
import it.unibo.kactor.sysUtil
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.junit.Before
import org.junit.Assert.assertEquals
import org.junit.Test

class TestPlasticCleanup {
	
	companion object {
		val EXPLORATION_WAIT_MS = 30 * 1000L

		val VISITED_MARKER = 1
		val PLASTIC_MARKER = 3
		
		val FIRST_PLASTIC_X = 1
		val FIRST_PLASTIC_Y = 1
		val SECOND_PLASTIC_X = 2
		val SECOND_PLASTIC_Y = 2
		
		val INITIAL_BOX_CONTENT = 0
		val FINAL_BOX_CONTENT = 2
	}
	
	var detector: ActorBasic? = null
	
	var roomHeight: Int? = null
	var roomWidth: Int? = null
	
	
	@Before
	@Throws(Exception::class)
	fun setUp() {
		GlobalScope.launch {
			it.unibo.ctxDetector.main()
		}

		Thread.sleep(2000)
		
		detector = sysUtil.getActor("detector")
	}
	
	@Test
	fun testPlasticCleanup() {
		readRoomDimensions()
		
		ensureCellsAreNotVisited()
		ensureDetectorBoxIsEmpty()
		Thread.sleep(EXPLORATION_WAIT_MS)
		ensureCellsAreVisited()
		ensureDetectorBoxContainsBottles()
	}
	
	fun readRoomDimensions() {
		with(detector!!) {
			solve("roomHeight(H)", "H")
			if (solveOk()) {
				roomHeight = resVar.toInt()
			}
			solve("roomWidth(W)", "W")
			if (solveOk()) {
				roomWidth = resVar.toInt()
			}
		}
	}
	
	
	fun ensureDetectorBoxIsEmpty() {
		with(detector!!) {
			solve("detector_box(S)", "S")
			if (solveOk()) {
				assertEquals("$INITIAL_BOX_CONTENT", resVar)
			}
		}
	}

	fun ensureDetectorBoxContainsBottles() {
		with(detector!!) {
			solve("detector_box(S)", "S")
			if (solveOk()) {
				assertEquals("$FINAL_BOX_CONTENT", resVar)
			}
		}
	}
	
	fun ensureCellsAreNotVisited() {
		for (y in 0..roomHeight!!-1) {
			for (x in 0..roomWidth!!-1) {
				with(detector!!) {
					solve("cell($y, $x, S)", "S")
					if (solveOk()) {
						assertEquals("0", resVar)
					}
				}
			}
		}
	}
	
	fun ensureCellsAreVisited() {
		for (y in 0..roomHeight!!-1) {
			for (x in 0..roomWidth!!-1) {
				with(detector!!) {
					solve("cell($y, $x, S)", "S")
					if (solveOk()) {
						assertEquals("1", resVar)
					}
				}
			}
		}
	}
	
}