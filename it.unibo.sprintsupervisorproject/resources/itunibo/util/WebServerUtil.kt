import java.net.URL
import java.net.HttpURLConnection
import java.io.BufferedReader
import java.io.InputStreamReader

object WebServerUtil{
	fun post(host: String, content: String) {
		val obj = URL(host);
		val conn = obj.openConnection() as HttpURLConnection;
		
		conn.setRequestMethod("POST");
		conn.setRequestProperty("User-Agent", "Mozilla/5.0");
		
		conn.setDoOutput(true);
		val os = conn.getOutputStream();
		os.write(content.toByteArray())
		os.flush();
		os.close();

		val responseCode = conn.getResponseCode();
		System.out.println("POST Response Code :: " + responseCode);
	}
}