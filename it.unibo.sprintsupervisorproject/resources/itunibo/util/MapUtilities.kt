package itunibo.util

object MapUtilities {
	var i = 0
	var j = 0
	var obstacles = emptyList<Pair<Int, Int>>().toMutableList()
	var explored = emptyList<Pair<Int, Int>>().toMutableList()
	var position = Pair<Int, Int>(0, 0)
	
	fun initialize(width: Int, height: Int) {
		i = width
		j = height
		explored.add(Pair<Int, Int>(0, 0))
	}
	
	fun generateCoordinates(width: Int, height: Int): List<Pair<Int, Int>> {
		val result = emptyList<Pair<Int, Int>>().toMutableList()
		for (x in 0..width-1) {
			for (y in 0..height-1) {
				result.add(Pair(x, y))
			}
		}
		return result
	}
	
	fun oneLineMap(): String {
		var result = ""
		for (y in 0..j-1) {
			for (x in 0..i-1) {
				if(obstacles.contains(Pair<Int, Int>(x, y))){
					result += "1"
				} else if (x == position.first && y == position.second){
					result += "R"
				} else if (explored.contains(Pair<Int, Int>(x, y))){
					result += "2"
				} else {
					result += "0"
				}
			}
			result += "@"
		}
		return result
	}
	
	fun updateObstacles(x: Int, y: Int) {
		obstacles.add(Pair<Int, Int>(x, y))
	}
	
	fun updatePosition(x: Int, y: Int) {
		position = Pair<Int, Int>(x, y)
	}
	
	fun updateExplored(x: Int, y: Int) {
		explored.add(Pair<Int, Int>(x, y))
	}	
}