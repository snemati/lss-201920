import org.junit.Test
import org.junit.Before
import it.unibo.kactor.ActorBasic
import it.unibo.kactor.sysUtil
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.junit.Assert.*

class TestEnvironmentConditions {
	companion object {
		val TEST_TIME_SECONDS = 30L
		val TEST_TIME_MS = TEST_TIME_SECONDS * 1000L
	}
	
	var worldObserver: ActorBasic? = null
	
	@Before
	@Throws(Exception::class)
	fun setUp() {
		GlobalScope.launch {
			it.unibo.ctxDetector.main()
		}
		
		Thread.sleep(5000)
		
		worldObserver = sysUtil.getActor("worldobserver")
	}
	
	@Test	
	fun testEnvironmentConditions() {
		Thread.sleep(TEST_TIME_MS)
		checkExploreContext()
		checkSuspendContext()
		checkTerminateContext()
	}
	
	fun checkExploreContext() {
		var i = 0
		with(worldObserver!!) {
			while(true) {
				solve("state_log(sendExplore, wroom_state(TVOC, _)), $i", "TVOC")
				if (solveOk()) {
					assertEquals("false", resVar)
				} else break
				solve("state_log(sendExplore, wroom_state(_, Open)), $i", "Open")
				if (solveOk()) {
					assertEquals("false", resVar)
				} else break
				i++
			}
		}
	}
	fun checkSuspendContext() {
		var i = 0
		with(worldObserver!!) {
			while(true) {
				solve("state_log(sendSuspend, wroom_state(_, Open)), $i", "Open")
				if (solveOk()) {
					assertEquals("false", resVar)
				} else break
				i++
			}
		}
	}
	fun checkTerminateContext() {
		var i = 0
		with(worldObserver!!) {
			while(true) {
				solve("state_log(sendTerminate, wroom_state(TVOC, _)), $i", "TVOC")
				if (solveOk()) {
					assertEquals("false", resVar)
				} else break
				solve("state_log(sendTerminate, wroom_state(_, Open)), $i", "Open")
				if (solveOk()) {
					assertEquals("false", resVar)
				} else break
				i++
			}
		}
	}
}