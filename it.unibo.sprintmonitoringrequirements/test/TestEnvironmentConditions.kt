import org.junit.Test
import org.junit.Before
import it.unibo.kactor.ActorBasic
import it.unibo.kactor.sysUtil
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.junit.Assert.*

class TestEnvironmentConditions {
	companion object {
		val TEST_TIME_SECONDS = 20L
		val TEST_TIME_MS = TEST_TIME_SECONDS * 1000L
	}
	
	var detector: ActorBasic? = null
	
	@Before
	@Throws(Exception::class)
	fun setUp() {
		GlobalScope.launch {
			it.unibo.ctxDetector.main()
		}
		
		Thread.sleep(5000)
		
		detector = sysUtil.getActor("detector")
	}
	
	@Test	
	fun testEnvironmentConditions() {
		Thread.sleep(TEST_TIME_MS)
		checkExploreContext()
		checkSuspendContext()
		checkTerminateContext()
	}
	
	fun checkExploreContext() {
		var i = 0
		with(detector!!) {
			while(true) {
				solve("state_log(executeExplore, wroom_state(TVOC, _)), $i", "TVOC")
				if (solveOk()) {
					assertEquals("false", resVar)
				} else break
				solve("state_log(executeExplore, wroom_state(_, Open)), $i", "Open")
				if (solveOk()) {
					assertEquals("false", resVar)
				} else break
				i++
			}
		}
	}
	fun checkSuspendContext() {
		var i = 0
		with(detector!!) {
			while(true) {
				solve("state_log(executeSuspend, wroom_state(_, Open)), $i", "Open")
				if (solveOk()) {
					assertEquals("false", resVar)
				} else break
				i++
			}
		}
	}
	fun checkTerminateContext() {
		var i = 0
		with(detector!!) {
			while(true) {
				solve("state_log(executeTerminate, wroom_state(TVOC, _)), $i", "TVOC")
				if (solveOk()) {
					assertEquals("false", resVar)
				} else break
				solve("state_log(executeTerminate, wroom_state(_, Open)), $i", "Open")
				if (solveOk()) {
					assertEquals("false", resVar)
				} else break
				i++
			}
		}
	}
}