%====================================================================================
% sprintboxproblem description   
%====================================================================================
context(ctxplasticbox, "localhost",  "TCP", "8017").
 qactor( plasticbox, ctxplasticbox, "it.unibo.plasticbox.Plasticbox").
  qactor( supervisormock, ctxplasticbox, "it.unibo.supervisormock.Supervisormock").
  qactor( detectormock, ctxplasticbox, "it.unibo.detectormock.Detectormock").
