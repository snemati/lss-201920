%====================================================================================
% sprintemptyrequirements description   
%====================================================================================
mqttBroker("localhost", "1883").
context(ctxdetector, "localhost",  "MQTT", "8016").
 qactor( detector, ctxdetector, "it.unibo.detector.Detector").
  qactor( plasticbox, ctxdetector, "it.unibo.plasticbox.Plasticbox").
  qactor( supervisor, ctxdetector, "it.unibo.supervisor.Supervisor").
