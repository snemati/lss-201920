%====================================================================================
% delivery description   
%====================================================================================
mqttBroker("localhost", "1883").
context(ctxdetector, "localhost",  "MQTT", "8016").
context(ctxplasticbox, "localhost",  "MQTT", "8017").
context(ctxsmartrobot, "127.0.0.1",  "MQTT", "8020").
 qactor( smartrobot, ctxsmartrobot, "external").
  qactor( worldobserver, ctxdetector, "it.unibo.worldobserver.Worldobserver").
  qactor( detector, ctxdetector, "it.unibo.detector.Detector").
  qactor( plasticbox, ctxdetector, "it.unibo.plasticbox.Plasticbox").
