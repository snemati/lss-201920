import java.net.URL
import java.net.HttpURLConnection
import java.io.BufferedReader
import java.io.InputStreamReader

object Supervisor{
	
	val host: String = "http://localhost:8080/"
	
	fun sendMap(map: String) {
		send("map", "map=" + map)
	}
	
	fun sendDetectorBoxAvailableSpace(space: String) {
		send("detector", "detector=" + space)
	}
	
	fun sendPlasticBoxAvailableSpace(space: String) {
		send("plastic", "plastic=" + space)
	}
	
	fun sendStuckAlarm() {
		send("stuck", "stuck=" + "stuck")
	}
	
	fun sendPlasticRequest(supposedPlastic: String) {
		send("isPlastic", "isPlastic=" + supposedPlastic)
	}
	
	fun sendEnvironmentReady() {
		send("ready", "ready=" + "ready")
	}
	
	fun test(test: String){
		send("test", "test=" + test)
	}
	
	private fun send(route: String, content: String){
		//check which one is the rightful send technology
		post(host + route, content)
		//println(host+route)
		//println(content)
	}
		
	private fun post(host: String, content: String) {
		val obj = URL(host);
		val conn = obj.openConnection() as HttpURLConnection;
		
		conn.setRequestMethod("POST");
		conn.setRequestProperty("User-Agent", "Mozilla/5.0");
		//println(conn)
		
		conn.setDoOutput(true);
		val os = conn.getOutputStream();
		os.write(content.toByteArray())
		os.flush();
		os.close();
		val res = conn.getResponseCode()
		//println(res.toString() + " " + content)
	}
}