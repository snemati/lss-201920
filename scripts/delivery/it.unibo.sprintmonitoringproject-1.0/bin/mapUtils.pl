map(Height,Width) :-
	range(0,Height,Y),
	range(0,Width,X),
	assert(cell(Y,X,0)).

robot_at_origin :-
	retract(cell(0, 0, X)),
	assert(cell(0, 0, 1)).

range(I,J,I) :-
	I < J.

range(I,J,K) :-
	I < J,
	I2 is I + 1,
	range(I2,J,K).