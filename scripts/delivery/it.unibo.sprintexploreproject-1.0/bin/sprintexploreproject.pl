%====================================================================================
% sprintexploreproject description   
%====================================================================================
mqttBroker("localhost", "1883").
context(ctxdetector, "localhost",  "MQTT", "8016").
context(ctxsmartrobot, "127.0.0.1",  "MQTT", "8020").
 qactor( smartrobot, ctxsmartrobot, "external").
  qactor( detector, ctxdetector, "it.unibo.detector.Detector").
  qactor( supervisor, ctxdetector, "it.unibo.supervisor.Supervisor").
