%====================================================================================
% sprintexploreproject description   
%====================================================================================
context(ctxdetector, "localhost",  "TCP", "8016").
context(ctxsmartrobot, "127.0.0.1",  "TCP", "8020").
 qactor( smartrobot, ctxsmartrobot, "external").
  qactor( detector, ctxdetector, "it.unibo.detector.Detector").
  qactor( movingmind, ctxdetector, "it.unibo.movingmind.Movingmind").
  qactor( mapresource, ctxdetector, "it.unibo.mapresource.Mapresource").
