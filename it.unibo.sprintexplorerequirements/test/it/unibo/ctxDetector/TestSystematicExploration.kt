package it.unibo.ctxDetector

import it.unibo.kactor.ActorBasic
import it.unibo.kactor.sysUtil
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.junit.Before
import org.junit.Test
import org.junit.Assert.assertEquals

class TestSystematicExploration {
	
	companion object {
		val EXPLORATION_WAIT_MS = 30 * 1000L
	}
	
	var detector: ActorBasic? = null
	
	var roomHeight: Int? = null
	var roomWidth: Int? = null
	
	@Before
	@Throws(Exception::class)
	fun setUp() {
		GlobalScope.launch {
			it.unibo.ctxDetector.main()
		}
		
		Thread.sleep(2000)
		
		detector = sysUtil.getActor("detector")
	}
	
	@Test
	fun testSystematicExploration() {
		readRoomDimensions()
		ensureCellsAreNotVisited()
		Thread.sleep(EXPLORATION_WAIT_MS)
		ensureCellsAreVisited()
	}
	
	fun readRoomDimensions() {
		with(detector!!) {
			solve("roomHeight(H)", "H")
			if (solveOk()) {
				roomHeight = resVar.toInt()
			}
			solve("roomWidth(W)", "W")
			if (solveOk()) {
				roomWidth = resVar.toInt()
			}
		}
	}
	
	fun ensureCellsAreNotVisited() {
		for (y in 0..roomHeight!!-1) {
			for (x in 0..roomWidth!!-1) {
				with(detector!!) {
					solve("cell($y, $x, S)", "S")
					if (solveOk()) {
						assertEquals("0", resVar)
					}
				}
			}
		}
	}
	
	fun ensureCellsAreVisited() {
		for (y in 0..roomHeight!!-1) {
			for (x in 0..roomWidth!!-1) {
				with(detector!!) {
					solve("cell($y, $x, S)", "S")
					if (solveOk()) {
						assertEquals("1", resVar)
					}
				}
			}
		}
	}
	
}